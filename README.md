# Fancy Timer App

Project to build my own timer app. 

## Hot Reload and Debugging 

Press F5 to start debug mode 
Press Ctrl + Shift + F5 to start / restart app

## VM zum Projekt (Buildozer)

Anleitung zur Erzeugung der VM nach https://www.youtube.com/watch?v=x5MhydijWmc&t=1112s
Name: Andre / andre-Virtualbox
Passwort: test123

Anleitung für Kivy und Buildozer: https://www.youtube.com/watch?v=zusbWcWddmA

Alle Files inklusive buildozer.spec befinden sich in /home/andre/TimerApp
    --> neu Erzeugen des APK: buildozer -v android debug
    --> Deployment + Starten der App: buildozer android deploy run (optional: logcat)

## Files und Sounds 

Coole motivierende Sounds nach Ablauf des Timers: https://www.melodyloops.com/music-for/sport/

Anmerkung: python-for-android unterstützt keine mp3-Files (Stand März 2021) --> Umwandlung in wav funktioniert

## Programming notes

### bind vs fbind
- bind: bindet callback an Element OHNE Argumente 
- fbind: bindet callback an Element MIT Argumenten
  - instance muss in der Funktionsdefinition als letztes stehen 
  - textinput funktioniert nur mit bind
- youtube link: https://www.youtube.com/watch?v=rdJAjEO-Oo8
