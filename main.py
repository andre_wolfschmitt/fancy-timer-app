# -*- coding: utf-8 -*-

from functools import partial

from kivy.app import App
from kivy.clock import Clock
from kivy.core.text import LabelBase
from kivy.core.window import Window
from kivy.utils import get_color_from_hex
from kivy.properties import ObjectProperty, ListProperty
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.lang import Builder
from kivy.core.audio import SoundLoader
from kivy.graphics import Color, RoundedRectangle

from plyer import vibrator

from time import strftime

from py_files.datamanager import DataManager
from py_files.edit_timer_screen import EditTimerScreen

Builder.load_file('kv_files/main_screen.kv')
Builder.load_file('kv_files/timesup_screen.kv')
Builder.load_file('kv_files/edit_sound_screen.kv')
Builder.load_file('kv_files/settings_screen.kv')


class BaseLayout(BoxLayout):
    pass


class TimerManager(ScreenManager):
    min_touch_length = 50
    screen_swiped = False

    def on_touch_move(self, touch):

        # do nothing if screen has already been changed since last touch down
        if (self.screen_swiped):
            return

        app = TimerApp.get_running_app()
        screen_count = len(app.screen_dictionary.keys())

        # detect swiping left
        if touch.ox - touch.x > self.min_touch_length:

            # get next screen index
            next_screen_index = app.screen_dictionary[app.current_screen] + 1

            # change screen to next one on the right
            if next_screen_index < screen_count:
                self.transition.direction = 'left'
                next_screen = app.get_screen_from_index(next_screen_index)
                if next_screen:
                    self.swipe_to_screen(next_screen)

        # detect swiping right
        elif touch.x - touch.ox > self.min_touch_length:

            # get next screen index
            next_screen_index = app.screen_dictionary[app.current_screen] + - 1

            # change screen to next one on the left
            if next_screen_index >= 0:
                self.transition.direction = 'right'
                next_screen = app.get_screen_from_index(next_screen_index)
                if next_screen:
                    self.swipe_to_screen(next_screen)

    def on_touch_up(self, touch):
        # reset variable to enable next swiping
        self.screen_swiped = False

    def swipe_to_screen(self, screen):
        app = TimerApp.get_running_app()
        self.current = screen
        app.current_screen = screen
        app.update_toolbar_styling()
        self.screen_swiped = True


class MainScreen(Screen):
    pass


class TimesUpScreen(Screen):
    pass


class EditSoundScreen(Screen):
    pass


class SettingsScreen(Screen):
    pass


class TimerLayout(BoxLayout):
    # time_prop = ObjectProperty(None)
    # sm_prop = ObjectProperty(None)

    def change_screen(self, screen):
        if (screen == 'stopwatch_screen'):
            # move toggle container to the left
            self.toggle_container_prop.pos = self.x, self.y
            # set stopwatch btn background
            self._sw_switch.background_color = 1, 1, 1, 0.1
            self._timer_switch.background_color = 0, 0, 0, 0        # set timer btn background
            # set stopwatch btn text color
            self._sw_switch.color = 1, 1, 1, 1
            # set timer btn text color
            self._timer_switch.color = 1, 1, 1, 0.5
        else:
            self.toggle_container_prop.pos = -self.width, self.y
            self._sw_switch.background_color = 0, 0, 0, 0
            self._timer_switch.background_color = 1, 1, 1, 0.1
            self._sw_switch.color = 1, 1, 1, 0.5
            self._timer_switch.color = 1, 1, 1, 1


class TimerOptionButton(Button):
    pass


class TimerApp(App):
    sw_seconds = 0
    sw_started = False
    timer_seconds = 0
    timer_started = False
    timer_timesup = False
    timer_alert_sound = None
    timers = []

    stop_btn_color = ListProperty([.9, .1, .1, .8])  # Red
    start_btn_color = ListProperty([.1, .8, .1, .7])  # Green

    current_screen = 'main_screen'
    screen_dictionary = {
        'main_screen': 0,
        'edit_sound_screen': 1,
        'edit_timer_screen': 2,
        'settings_screen': 3
    }

    def build(self):
        self.timer_alert_sound = SoundLoader.load(
            'sound/melodyloops-preview-lets-rock-it-1m34s.wav')

    def on_start(self):
        # load existing timers from txt file
        self.timers = DataManager.load_timers()
        self.fill_timer_options_container()

        # init toolbar with correct highlighting
        self.update_toolbar_styling()

        # init screens
        self.root._edit_timer_screen.init()

        Clock.schedule_interval(self.update_time, 0)

    def set_timestring(self, sec):
        # minutes = self.sw_seconds / 60, seconds = self.sw_seconds % 60
        minutes, seconds = divmod(sec, 60)
        time_str = (
            '%02d:%02d.[size=60]%02d[/size]' %
            (int(minutes), int(seconds), int(seconds * 100 % 100))
        )
        return time_str

    def update_time(self, nap):
        # update clock
        self.root._main_screen.time_prop.text = strftime('[b]%H[/b]:%M:%S')

        # update stopwatch
        if self.sw_started:
            self.sw_seconds += nap

        self.root._main_screen.stopwatch_prop.text = self.set_timestring(
            self.sw_seconds)

        # update timer
        if self.timer_started and self.timer_seconds > 0:
            self.timer_seconds -= nap

        self.root._main_screen.timer_display_prop.text = self.set_timestring(
            self.timer_seconds)

        # make sure timer does not display values < 0 and call timer_done
        if self.timer_seconds < 0:
            self.timer_seconds = 0
            # self.root._main_screen.timer_display_prop.text = '00.00.[size=60]00[/size]'
            self.timer_started = False
            self.root._main_screen.start_stop_timer_prop.text = 'Start'
            self.timer_timesup = True
            self.timer_done()

    def start_stop(self):
        self.root._main_screen.start_stop_prop.text = (
            'Start' if self.sw_started else 'Stop'
        )
        self.sw_started = not self.sw_started

    def reset(self):
        self.sw_seconds = 0

    def set_timer(self, sec):
        if (self.timer_started):
            self.timer_started = False
        self.timer_seconds = sec
        self.root._main_screen.timer_display_prop.text = self.set_timestring(
            self.timer_seconds)

    def start_stop_timer(self):

        # only switch between started and stopped if timer sec > 0
        if self.timer_seconds > 0:
            self.timer_started = not self.timer_started
        else:
            self.timer_started = False

        # update styling
        self.update_start_stop_btn()

    def reset_timer(self):
        self.timer_started = False
        self.set_timer(90)
        self.update_start_stop_btn()

    # function to react on timer done event
    def timer_done(self):
        # execute as long as user does not push the button
        if self.timer_timesup:
            self.root._timer_manager.current = "timesup_screen"

            # try to let the device vibrate
            try:
                vibrator.vibrate(1)
                # use lambda to call timer_done with argument dt
                Clock.schedule_once(lambda dt: self.timer_done(), 1.5)
            except:
                print('ERROR: plyer functionality not available')

            # try to alert with sound
            if (self.timer_alert_sound and self.timer_alert_sound.state == 'stop'):
                self.timer_alert_sound.play()

    # stop timer alert + go back to main screen
    def shut_up_timer(self):
        self.timer_timesup = False
        if (self.timer_alert_sound and self.timer_alert_sound.state == 'play'):
            self.timer_alert_sound.stop()
        self.root._timer_manager.current = 'main_screen'
        self.timer_started = False
        self.update_start_stop_btn()

    # generate correct callback for timer option button
    def set_timer_option(self, sec_value, btn):
        self.set_timer(sec_value)
        self.update_start_stop_btn()

    # switch between screens
    def switch_screen(self, screen):

        # skip if screen already up to date
        if (self.current_screen == screen):
            return

        # switch screen + adjust toolbar
        self.update_screen_transition(screen)
        self.current_screen = screen
        self.root._timer_manager.current = screen
        self.update_toolbar_styling()

    def update_screen_transition(self, screen):
        # look up index in screen dictionary
        idx_upcoming_screen = self.screen_dictionary[screen]
        idx_current_screen = self.screen_dictionary[self.current_screen]

        # compare index to detect transition direction
        if (idx_current_screen < idx_upcoming_screen):
            self.root._timer_manager.transition.direction = 'left'
        else:
            self.root._timer_manager.transition.direction = 'right'

    def get_screen_from_index(self, idx):
        for screen, index in self.screen_dictionary.items():
            if (index == idx):
                return screen
        return None

# --------------------------------------------------------------------------------------------
# functions to modify view

    # generate timer option from timerdata file
    def fill_timer_options_container(self):
        i = 1
        for timer in self.timers:
            # create new timer option button instance
            new_btn = TimerOptionButton(text='{min}:{sec}'.format(
                min=timer['min'], sec=timer['sec']))
            new_btn.id = 'timer{i}'.format(i=i)
            i += 1

            # bind callback to new created button
            sec_value = float(timer['min']) * 60 + float(timer['sec'])

            # callbacks with parameters
            new_btn.fbind("on_release", self.set_timer_option, sec_value)
            # new_btn.bind(
            #     on_release=lambda btn:
            #         self.set_timer_option(btn))

            # add new button to layout
            self.root._main_screen._timer_option_container.add_widget(new_btn)

    # update styling of buttons in toolbar
    def update_toolbar_styling(self):

        # get btn from toolbar to hightlight
        btn = self.root._tb_home_btn
        if self.current_screen == 'edit_sound_screen':
            btn = self.root._tb_sound_btn
        elif self.current_screen == 'edit_timer_screen':
            btn = self.root._tb_timer_btn
        elif self.current_screen == 'settings_screen':
            btn = self.root._tb_settings_btn

        # adjust styling
        btn.color = 1, 1, 1, 1
        btn.bold = True

        # reset highlighting of other buttons
        all_btns = [self.root._tb_home_btn, self.root._tb_sound_btn,
                    self.root._tb_timer_btn, self.root._tb_settings_btn]
        for b in all_btns:
            if b is not btn:
                b.color = 1, 1, 1, 0.5
                b.bold = False

    # update styling of start / stop button
    def update_start_stop_btn(self):
        btn = self.root._main_screen.start_stop_timer_prop

        # clear background of button
        btn.canvas.before.clear()

        with btn.canvas.before:
            Color(rgba=self.stop_btn_color) if self.timer_started else Color(
                rgba=self.start_btn_color)
            RoundedRectangle(pos=btn.pos, size=btn.size, radius=[50])

        btn.text = (
            'Stop' if self.timer_started else 'Start'
        )


if __name__ == '__main__':
    Window.clearcolor = get_color_from_hex('#101216')
    LabelBase.register(name='Roboto',
                       fn_regular='roboto/Roboto-Thin.ttf',
                       fn_bold='roboto/Roboto-Medium.ttf')
    TimerApp().run()
