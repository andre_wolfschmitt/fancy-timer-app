import json


class DataManager:

    def load_timers():
        datafile = open('timerdata.txt', 'r+')
        text = datafile.read()
        timer_data = json.loads(text)
        return timer_data["timers"]
