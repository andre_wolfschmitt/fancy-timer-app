from kivy.lang import Builder
from kivy.uix.screenmanager import Screen
from kivy.uix.button import Button

from py_files.datamanager import DataManager

Builder.load_file('kv_files/edit_timer_screen.kv')


class EditTimerScreen(Screen):
    existing_timers = None

    def init(self):
        self.existing_timers = DataManager.load_timers()
        self.init_edit_timer_dashboard()

    # init existing timers
    def init_edit_timer_dashboard(self):
        for timer in self.existing_timers:
            newBtn = Button(
                text=timer['name'] + '\n' + timer['min'] + ' : ' + timer['sec']
            )
            self._edit_timer_dashboard.add_widget(newBtn)
